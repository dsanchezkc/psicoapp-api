<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once( APPPATH.'/libraries/REST_Controller.php' );
use Restserver\libraries\REST_Controller;


class Registro extends REST_Controller {


  public function __construct(){

    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
    header("Access-Control-Allow-Origin: *");


    parent::__construct();
    $this->load->database();

  }


  public function index_get(){

    // Usuario y Token son correctos
    $this->db->reset_query();

    $insertar = array('nombre' => $_GET['nombre'],'email' => $_GET['email'], 'edad' => $_GET['edad'] , 'genero' => $_GET['genero'], 'pais' => $_GET['pais'], 'etnia' => $_GET['etnia'], 'estudios' => $_GET['estudios']);
    $this->db->insert( 'usuario', $insertar );
   
    $respuesta = array(
                  'error' => FALSE,
                );
    $this->response( $respuesta );

  }
}

