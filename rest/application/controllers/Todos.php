<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once( APPPATH.'/libraries/REST_Controller.php' );
use Restserver\libraries\REST_Controller;


class Todos extends REST_Controller {


  public function __construct(){

    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
    header("Access-Control-Allow-Origin: *");

    parent::__construct();
    $this->load->database();

  }

  public function index_get( ){

    $ids = $_GET['ids'];
    $sintomas = substr($ids, 1, -1); 
    $array = array_map('intval', explode(',', $sintomas));

    $getFlujos = $this->db->query('SELECT flujo.id, flujo.nombre, flujo.primera_pregunta_id, flujo.tipo_flujo, COUNT(flujosintoma.sintoma_id) FROM flujo JOIN flujosintoma ON flujo.id = flujosintoma.flujo_id WHERE flujosintoma.sintoma_id IN ('.implode(', ', $array) .') GROUP BY flujosintoma.flujo_id ORDER BY COUNT(flujosintoma.sintoma_id), flujo.id DESC')->result_array();
    $result = array();

     foreach ($getFlujos as $row)
     {
        $preguntas = array();
        $getPreguntas = $this->db
                ->query('select Pregunta.id, Pregunta.texto,Pregunta.texto_en ,Pregunta.flujo_id, Pregunta.subpregunta,Pregunta.subpregunta_en ,Pregunta.categoria from Pregunta where Pregunta.flujo_id ='. $row['id'])
                ->result_array();

        foreach ($getPreguntas as $pregunta) {
          $respuesta = $this->db
                ->query('select Respuesta.id, Respuesta.texto,Respuesta.texto_en ,Respuesta.pregunta_siguiente_id, Respuesta.diagnostico_id, Respuesta.peso from Respuesta where Respuesta.pregunta_id ='. $pregunta['id'])
                ->result_array();

          $pregunta = array(
              'id' => $pregunta['id'],
              'texto' => $pregunta['texto'],
	      'texto_en' => $pregunta['texto_en'],
	      'flujo_id' => $pregunta['flujo_id'],
	      'categoria' => $pregunta['categoria'],
              'respuesta' => $respuesta,
	      'subpregunta' => $pregunta['subpregunta'],
	      'subpregunta_en' => $pregunta['subpregunta_en']
          );

          array_push($preguntas, $pregunta );
        }

        $respuesta = array(
          'id' => $row['id'],
          'nombre' => $row['nombre'],
	  'primera_pregunta' =>$row['primera_pregunta_id'],
	  'tipo_flujo' => $row['tipo_flujo'],
          'preguntas' => $preguntas
        );
        array_push($result, $respuesta );

     }

     $json = array("data" => $result);

    $this->response( $json );
  }

 public function flujos_get( ){

    $id = $_GET['id'];
    $sintomas = substr($id, 1, -1);
    $array = array_map('intval', explode(',', $sintomas));

    $getFlujos = $this->db->query('SELECT flujo.id, flujo.nombre, flujo.primera_pregunta_id, flujo.tipo_flujo  FROM flujo WHERE flujo.id ='. $id)->result_array();
    $result = array();

     foreach ($getFlujos as $row)
     {
        $preguntas = array();
        $getPreguntas = $this->db
                ->query('select Pregunta.id, Pregunta.texto,Pregunta.texto_en ,Pregunta.flujo_id, Pregunta.subpregunta,Pregunta.subpregunta_en ,Pregunta.categoria from Pregunta where Pregunta.flujo_id ='. $row['id'])
                ->result_array();

        foreach ($getPreguntas as $pregunta) {
          $respuesta = $this->db
                ->query('select Respuesta.id, Respuesta.texto,Respuesta.texto_en ,Respuesta.pregunta_siguiente_id, Respuesta.diagnostico_id, Respuesta.peso from Respuesta where Respuesta.pregunta_id ='. $pregunta['id'])
                ->result_array();

          $pregunta = array(
              'id' => $pregunta['id'],
              'texto' => $pregunta['texto'],
              'texto_en' => $pregunta['texto_en'],
              'flujo_id' => $pregunta['flujo_id'],
              'categoria' => $pregunta['categoria'],
              'respuesta' => $respuesta,
              'subpregunta' => $pregunta['subpregunta'],
              'subpregunta_en' => $pregunta['subpregunta_en']
          );

          array_push($preguntas, $pregunta );
        }

        $respuesta = array(
          'id' => $row['id'],
          'nombre' => $row['nombre'],
          'primera_pregunta' =>$row['primera_pregunta_id'],
          'tipo_flujo' => $row['tipo_flujo'],
          'preguntas' => $preguntas
        );
        array_push($result, $respuesta );

     }

     $json = array("data" => $result);

    $this->response( $json );
  }

}

