<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once( APPPATH.'/libraries/REST_Controller.php' );
use Restserver\libraries\REST_Controller;


class Usuario extends REST_Controller {


  public function __construct(){

    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
    header("Access-Control-Allow-Origin: *");

    parent::__construct();
    $this->load->database();

  }

  public function index_get( ){

    $email = $_GET['email'];

    $result = $this->db->query('SELECT usuario.id, usuario.genero  FROM usuario WHERE usuario.email ="' .$email.'"')->result_array();

     $json = array($result);

    $this->response( $json );
  }

}
