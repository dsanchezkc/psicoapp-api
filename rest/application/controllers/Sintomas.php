<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once( APPPATH.'/libraries/REST_Controller.php' );
use Restserver\libraries\REST_Controller;


class Sintomas extends REST_Controller {


  public function __construct(){

    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
    header("Access-Control-Allow-Origin: *");

    parent::__construct();
    $this->load->database();

  }

  public function index_get( ){

    $query = $this->db->query('SELECT * FROM `sintoma`');

    $respuesta = array(
            'error' => FALSE,
            'sintomas' => $query->result_array()
          );

    $this->response( $respuesta );
  }
  
  public function buscar_get( $termino = "" ){

    // LIKE
    $query = $this->db->query("SELECT * FROM `sintoma` where texto like '%".$termino."%'");

    $respuesta = array(
            'error' => FALSE,
            'termino'=> $termino,
            'sintomas' => $query->result_array()
          );

    $this->response( $respuesta );

  }

  public function buscar_en_get( $termino = "" ){

    // LIKE
    $query = $this->db->query("SELECT * FROM `sintoma` where texto_en like '%".$termino."%'");

    $respuesta = array(
            'error' => FALSE,
            'termino'=> $termino,
            'sintomas' => $query->result_array()
          );

    $this->response( $respuesta );

  }


}
                                  


