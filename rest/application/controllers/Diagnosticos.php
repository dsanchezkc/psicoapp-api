<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once( APPPATH.'/libraries/REST_Controller.php' );
use Restserver\libraries\REST_Controller;


class Diagnosticos extends REST_Controller {

  public function __construct(){
    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
    header("Access-Control-Allow-Origin: *");

    parent::__construct();
    $this->load->database();

  }

  public function todos_get( ){

    $ids = $_GET['ids'];
    $sintomas = substr($ids, 1, -1);
    $array = array_map('intval', explode(',', $sintomas));

    $getDiagnosticos = $this->db->query('SELECT diagnostico.id, diagnostico.titulo, diagnostico.titulo_en ,diagnostico.descripcion, diagnostico.descripcion_en, diagnostico.reporte,diagnostico.reporte_en,diagnostico.causas,diagnostico.causas_en,diagnostico.factores,diagnostico.factores_en,diagnostico.complicaciones,diagnostico.complicaciones_en, diagnostico.incidencias  FROM diagnostico  WHERE diagnostico.id IN ('.implode(', ', $array) .') ')->result_array();
    $result = array();

     foreach ($getDiagnosticos as $row)
     {
        $respuesta = array(
          'id' => $row['id'],
          'titulo' => $row['titulo'],
	  'titulo_en' => $row['titulo_en'],
          'descripcion' =>$row['descripcion'],
	  'descripcion_en' => $row['descripcion_en'], 'reporte' => $row['reporte'], 'reporte_en' => $row['reporte_en'], 'causas' => $row['causas'], 'causas_en' => $row['causas_en'], 'factores' => $row['factores'], 'factores_en' => $row['factores_en'], 'complicaciones' => $row['complicaciones'], 'complicaciones_en' => $row['complicaciones_en'], 'incidencias'=> $row['incidencias']
	
        );
        array_push($result, $respuesta );
     }

     $json = array("data" => $result);

    $this->response( $json );
  }
  public function ingresar_diagnostico_post($email ){

    $data = $this->post();

    if( !isset( $data["diagnosticos"] )  ){
      $respuesta = array(
                    'error' => TRUE,
                    'mensaje'=> "Faltan los items en el post"
                  );
      $this->response( $respuesta, REST_Controller::HTTP_BAD_REQUEST );
      return;
    }

    $items =  $data['diagnosticos'];
    $query = $this->db->query('SELECT id FROM `usuario` where email = "' . $email.'"' );
    foreach( $query->result() as $row ){
	$usuario_id= $row->id;
    }

    foreach( $items as $diagnostico ){
      $data_insertar = array( 'usuario_id' => $usuario_id, 'diagnostico_id'=> $diagnostico );
      $this->db->insert('diagnosticousuario', $data_insertar);
    }

    $respuesta = array(
                  'error' => FALSE
    );
    $this->response( $respuesta );
  }
}  


