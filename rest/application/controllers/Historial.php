<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once( APPPATH.'/libraries/REST_Controller.php' );
use Restserver\libraries\REST_Controller;


class Historial extends REST_Controller {


  public function __construct(){

    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
    header("Access-Control-Allow-Origin: *");

    parent::__construct();
    $this->load->database();

  }

  public function index_get( ){

    $email = $_GET['email'];

    $result = $this->db->query('SELECT  diagnostico.id, diagnostico.titulo, diagnostico.titulo_en , diagnostico.descripcion, diagnostico.descripcion_en,DATE(diagnosticousuario.fecha_diagnostico) as fecha_diagnostico,diagnostico.reporte, diagnostico.reporte_en, diagnostico.causas,diagnostico.causas_en,diagnostico.factores,diagnostico.factores_en,diagnostico.complicaciones,diagnostico.complicaciones_en  FROM diagnostico JOIN diagnosticousuario ON diagnosticousuario.diagnostico_id = diagnostico.id JOIN usuario ON diagnosticousuario.usuario_id = usuario.id WHERE usuario.email ="' .$email.'" order by diagnosticousuario.fecha_diagnostico desc')->result_array();

     $json = array($result);

    $this->response( $result );
  }

}

